﻿using UnityEngine;
using System.Collections;

public class Ghost : MonoBehaviour {

	protected int score;
	protected GameObject gameManager;
	protected float animationSpeed = 1.0f;
	protected bool hit = false;
	protected GameObject holder;


	protected virtual void Awake() {

	}
		

	protected virtual void Start () {
		gameManager = GameObject.Find("GameManager");
		holder = this.transform.parent.gameObject; //this is the parent object
		holder.GetComponent<Animator>().speed = animationSpeed;
	}
	
	// Update is called once per frame
	protected virtual void Update () {
		if(holder.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime>=0.95f) {
			Destroy(holder.gameObject);
		}
	}
	
	protected virtual void OnMouseDown() {
		hit = true;
		this.GetComponent<Collider>().enabled = false;
		Color _tmp = this.GetComponent<Renderer>().material.color;
		_tmp.a = 0.3f;
		this.GetComponent<Renderer>().material.color = _tmp;
		gameManager.GetComponent<GameManager>().incrementScore(score);
	}
}
