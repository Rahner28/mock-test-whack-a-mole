﻿using UnityEngine;
using System.Collections;

public class RedGhost : Ghost {
	
	protected override void Awake () {
		base.Awake ();
		score = 20;
		animationSpeed=1.1f;
	}
	
	protected override void Start() {
		base.Start ();
		this.GetComponent<Renderer>().material.color = new Color(1,0,0);
	}

	protected override void Update() {
		base.Update();
		if(hit) holder.transform.Translate(Vector3.up*3*Time.fixedDeltaTime, Space.World);
		if(holder.transform.position.y>5) { 
			Destroy (holder.gameObject);
		}
	}


	protected override void OnMouseDown() {
		base.OnMouseDown();
		holder.GetComponent<Animator>().speed=0;
		holder.GetComponent<Animator>().StopPlayback();
	}
}
